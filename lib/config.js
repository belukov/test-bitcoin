'use strict';

const nconf = require('nconf');

nconf.argv()
  .env()
  .file('default', {
    file: './var/conf.json'
  });

module.exports = nconf;
