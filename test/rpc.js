'use strict';

const rpc = require('../lib/rpcclient');

describe("test client", () => {

  var defaultAddress = null;
  var testAddr = null;

  before((done) => {
  
    rpc.getAccountAddress('', (err, res) => {
      if (err) return done(err);
      console.log('main addr: ', res);
      defaultAddress = res.result;
      return done();
    });
  });


  it("Make new address and account", (done) => {
 


    rpc.getNewAddress( (err, res) => {
    
      if (err) return done(err);
      console.log('new address: ', res);
      testAddr = res.result;
      rpc.setAccount(testAddr, 'test_1', (err, res) => {
        if (err) return done(err);
        console.log('set acc: ', res);
        return done();
      });
    });
  });


  it("Send some money ", (done) => {
  
    rpc.sendFrom('', testAddr, 2, (err, res) => {
      if (err) return done(err);

      console.log('res: ', res);
      return done();
    })
  });


  it('Confirm transactions', (done) => {
    rpc.generate(1, (err, res) => {
      if (err) return done(err);
      console.log('generate res: ', res);
      return done();
    });
  })


  it("get balance(?)", (done) => {
  
    rpc.getReceivedByAddress(testAddr, (err, balance) => {
    
      console.log("balance: ", balance);
      return done();
    });

  });

  it('List transactions', (done) => {
  
    rpc.listTransactions('test_1', (err, res) => {
      if (err) return done(err);
      console.log('listtransactions: ', res);
      return done();
    })
  });

  it('Account list', (done) => {
    rpc.listAccounts((err, accs) => {
      if (err) return done(err);
      console.log('accs: ', accs);
      return done();
    });
 
  });

  
});
