'use strict';

const mysql = require('mysql');
const conf = require('./config');
const connection = mysql.createConnection(conf.get('mysqlOptions'));

module.exports = connection;
