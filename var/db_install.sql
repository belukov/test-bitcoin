

DROP table if exists transactions;

CREATE table transactions (
  id int(11) AUTO_INCREMENT,
  txid varchar(64) not null,
  time int,
  amount float (10, 8),
  category varchar(64),
  address varchar(40),
  blockhash varchar(64) not null,

  primary key (id),
  unique (txid)

);

DROP table if exists address_summary;

CREATE table address_summary (
  id int(11) AUTO_INCREMENT,
  address varchar(40) not null,
  balance float(10, 8) default 0,
  transactions_count int default 0,
  
  primary key (id),
  unique (address)
);
