'use strict';

const debug = require('debug')('test-bitcoin:lib:rpcApi');
const RpcClient = require('bitcoind-rpc');
const conf = require('./config');

const opts = conf.get('rpcOptions');

const rpc = new RpcClient(opts);

const addr = conf.get('bitcoinAddress');
const acc = 'acc_' + addr;

class rpcApi {

  constructor() {
    this.rpc = rpc;
  }

  btcGetTransactionsAfter (lastTime = 0, from = 0) {

    return new Promise((resolve, reject) => {

      let res = [];

      debug("btcGetTransactionsAfter(%s, %s)", lastTime, from);
      this.rpc.listTransactions(acc, 10, from, async (err, result) => {
        //debug('btcGetTransactionsAfter res: ', err, result);
        if (err) return reject(err);
        res = result.result.filter((item) => {
          return !lastTime || (lastTime < item.time);
        });
        debug('list count: ', res.length);

        if (res.length >= 10) {
          let _next = await this.btcGetTransactionsAfter(lastTime, from + 10);
          res = [...res, ..._next];
        }

        return resolve(res.filter((item) => {return item.confirmations > 0}));

      });
    });
  }

  getBalance() {
    return new Promise((resolve, reject) => {

      this.rpc.getBalance(acc, (err, res) => {
        if (err) return reject(err);
        debug('new balance: ', res.result);

        return resolve(res.result);
      });
    });
  }

}


module.exports = new rpcApi;


