'use strict';

const debug = require('debug')('test-bitcoin:lib:address_control');

const conf = require('./config');
const rpcApi = require('./rpcclient');
const db = require('./mysql');

const addr = conf.get('bitcoinAddress');
const acc = 'acc_' + addr;

let status = 'pending';

function getStatus() {
  return status;
};

function getSummary() {
  return new Promise(async (resolve, reject) => {
    let res = await dbQuery('SELECT * from address_summary where address = ?', addr);
    debug('summary: ', res);
    return resolve(res[0] || {});
  });
}

function getTransactions() {
  return dbQuery("select * from transactions where address = ? order by time", addr);
}

module.exports = {getStatus, getSummary, getTransactions}


// Дальше идут функции для регулярного обновления информации в БД...

function checkout() {

  status = 'pending';
  debug('checkout...');
  checkSummary()
    .then(loadTransactions)
    .then(updateSummary)
    .catch((e) => {
      console.error("ERR: ", e.message);
      debug("checkout error: ", e);
      return err;
    })
    .then((res) => {
      status = res === true ? 'ready' : 'error';
      setTimeout(checkout, 10000);
    });
}

async function checkSummary() {
  
  let _res = await dbQuery('select 1 from `address_summary` where address = ?', addr);
  debug('check summary res: ', _res)
  if (_res.length) return true;

  await dbQuery('insert into `address_summary` (address) values (?)', addr);
  return true;
}

async function updateSummary(newTransactionsCount) {
  debug('updateSummary(%s)', newTransactionsCount);

  //if (!newTransactionsCount) return resolve();

  let _transCount = await dbQuery('SELECT count(0) cnt FROM `transactions` WHERE address = ?', addr);
  let transCount = _transCount[0].cnt;

  let balance = await rpcApi.getBalance();
  debug('new balance: ', balance);

  
  await dbQuery("UPDATE `address_summary` SET transactions_count = ?, balance = ?", [transCount, balance]);

  return true;
}

async function loadTransactions() {

  debug('start loadTransactions');
  let lastTime = 0;
  let _lastTime = await dbQuery("select time from `transactions` where address = ? order by time desc limit 1", addr);
  
  if (_lastTime.length) {
    lastTime = _lastTime[0].time;
  }
  debug('lastTime: ', lastTime);
  let transactions = await rpcApi.btcGetTransactionsAfter(lastTime);

  if (!transactions.length) return 0;

  let sql = "INSERT INTO `transactions` (txid, time, amount, category, address, blockhash) VALUES ";
  sql += transactions
    .reverse()
    .map((item) => {
      return "('"+item.txid+"', "
        + item.time + ', '
        + item.amount + ', '
        + "'" + item.category + "', "
        + "'" + item.address  + "', "
        + "'" + item.blockhash + "'"
        + ")";
    }).join(', ');
  debug("sql: ", sql);
  await dbQuery(sql);

  return transactions.length;
}

function dbQuery(sql, arg = []) {
  debug("dbQuery(%s, %s)", sql, arg);
  return new Promise((resolve, reject) => {
    db.query(sql, arg, (err, res) => {
      if (err) return reject(err);
      return resolve(res);
    });
  });
}

checkout();

