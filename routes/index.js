'use strict';

const debug = require('debug')('test-bitcoin:route:index');
const express = require('express');
const router = express.Router();

const conf = require('../lib/config');
const ctrl = require('../lib/address_control');

/* GET home page. */
router.get('/', async (req, res, next) => {

  let locals = {
    bitcoinAddress: conf.get('bitcoinAddress')
  };

  locals.status = ctrl.getStatus();
  locals.summary = await ctrl.getSummary();

  debug('locals: ', locals);
  res.render('index', locals);
});

router.get('/transactions', async (req, res, next) => {

  let locals = {};

  locals.transactions = await ctrl.getTransactions() || [];
  debug('transactions: ', locals.transactions);

  res.render('transactions', locals);
});

module.exports = router;
